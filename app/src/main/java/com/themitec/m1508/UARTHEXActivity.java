package com.themitec.m1508;

import androidx.appcompat.app.AppCompatActivity;

import com.mobiletek.uart.UARTManager;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;
import android.widget.AdapterView;
import java.util.Arrays;

public class UARTHEXActivity extends AppCompatActivity {
    private static final String TAG = "UARTHEXActivity";

    private UARTManager uartManager;
    private  int count = 0 ;
    private Button buttonRead_hex;
    private Button buttonWrite_hex;
    private EditText editText_hex;
    private TextView textView_hex;
    private Spinner baud_rate_hex;
    private Spinner path_hex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uarthex);
        init();
    }


    void init()
    {
        path_hex = (Spinner) findViewById(R.id.path_spinner_hex);
        baud_rate_hex = (Spinner) findViewById(R.id.baud_rate_spinner_hex);

        buttonRead_hex = (Button) findViewById(R.id.button_hex);
        buttonWrite_hex = (Button) findViewById(R.id.button2_hex);

        editText_hex = (EditText) findViewById(R.id.editText_hex);
        textView_hex = (TextView) findViewById(R.id.textView_hex);

        ArrayAdapter path_adapter = ArrayAdapter.createFromResource(
                this, R.array.path, android.R.layout.simple_spinner_item);
        path_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        path_hex.setAdapter(path_adapter);
        setSpinnerItemSelectedByValue(path_hex, UARTManager.MSM0_PATH);

        ArrayAdapter baud_rate_adapter = ArrayAdapter.createFromResource(
                this, R.array.baud_rate, android.R.layout.simple_spinner_item);
        baud_rate_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        baud_rate_hex.setAdapter(baud_rate_adapter);
        setSpinnerItemSelectedByValue(baud_rate_hex, String.valueOf(UARTManager.BAUD_RATE));

        uartManager = UARTManager.getInstance(this);

        buttonRead_hex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                uartManager.readDataByte(new UARTManager.OnDataByteReceiveListener() {
                    @Override
                    public void invoke(byte[] bytes, int length){
                        StringBuilder buf = new StringBuilder();
                        for(int i = 0; i < length; i ++)
                        {
                            buf.append(String.format("%02x ", bytes[i]));
                        }

                        textView_hex.setText(String.format("len=%d, bcall %d times %n", length, count) + buf.toString());

                        count ++;
                    }
                });

                textView_hex.setText("begin receive");
            }
        });

        path_hex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String device_path = path_hex.getSelectedItem().toString();
                Log.i(TAG, device_path);
                uartManager.changeDeviceAndBaudRate(device_path, 0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        buttonWrite_hex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editable text = editText_hex.getText();
                if (text != null && !text.toString().isEmpty()) {
                    /*
                    if (dataType) {
                        String buffer = text.toString();
                        uartManager.writeData(buffer);
                    }else { */
                    String buffer[] = text.toString().split(" ");
                    StringBuilder  buffer1 = new StringBuilder();

                    for (int i = 0; i < buffer.length; i ++)
                    {
                        if (buffer[i].length() > 2) {
                            buffer1.append(buffer[i].substring(0, 2));
                        }
                        else
                        {
                            buffer1.append(buffer[i]);
                        }
                        buffer1.append(' ');
                    }
                    editText_hex.setText(buffer1.toString());

                    String buffer2[] = buffer1.toString().split(" ");
                    byte[] bytes = new  byte[buffer2.length];
                    Arrays.fill(bytes, (byte) 0);
                    for (int i = 0; i < buffer.length; i ++)
                    {
                        bytes[i] = (byte)Integer.parseInt(buffer2[i], 16);
                    }

                    uartManager.writeDataByte(bytes);

                    uartManager.readDataByte(new UARTManager.OnDataByteReceiveListener() {
                        @Override
                        public void invoke(byte[] bytes, int length){
                            StringBuilder buf = new StringBuilder();
                            for(int i = 0; i < length; i ++)
                            {
                                buf.append(String.format("%02x ", bytes[i]));
                            }

                            textView_hex.setText(String.format("len=%d, bcall %d times %n", length, count) + buf.toString());

                            count ++;
                        }
                    });
                } else {
                    Toast.makeText(UARTHEXActivity.this, R.string.please_input_data, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void setSpinnerItemSelectedByValue(Spinner spinner,String value){
        SpinnerAdapter apsAdapter= spinner.getAdapter();
        int k= apsAdapter.getCount();
        for(int i=0;i<k;i++){
            if(value.equals(apsAdapter.getItem(i).toString())){
                spinner.setSelection(i,true);
                break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uartManager.release();
    }

}
