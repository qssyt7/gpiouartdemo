package com.themitec.m1508;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.mobiletek.uart.UARTManager;

public class UARTTestActivity extends Activity {
    private static final String TAG = "UARTTestActivity";

    private Button buttonRead;
    private Button buttonWrite;
    private EditText editText;
    private TextView textView;
    private Spinner baud_rate;
    private Spinner path;
    private UARTManager uartManager;

    // private  byte[] bytes = new  byte[128];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uarttest);
        Log.i(TAG, "go to onCreate() method");
        init();
    }

    public void init(){
        path = (Spinner) findViewById(R.id.path_spinner);
        baud_rate = (Spinner) findViewById(R.id.baud_rate_spinner);

        buttonRead = (Button) findViewById(R.id.button);
        buttonWrite = (Button) findViewById(R.id.button2);

        editText = (EditText) findViewById(R.id.editText);
        textView = (TextView) findViewById(R.id.textView);

        ArrayAdapter path_adapter = ArrayAdapter.createFromResource(
                this, R.array.path, android.R.layout.simple_spinner_item);
        path_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        path.setAdapter(path_adapter);
        setSpinnerItemSelectedByValue(path, UARTManager.MSM0_PATH);

        ArrayAdapter baud_rate_adapter = ArrayAdapter.createFromResource(
                this, R.array.baud_rate, android.R.layout.simple_spinner_item);
        baud_rate_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        baud_rate.setAdapter(baud_rate_adapter);
        setSpinnerItemSelectedByValue(baud_rate, String.valueOf(UARTManager.BAUD_RATE));

        uartManager = UARTManager.getInstance(this);

        path.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String device_path = path.getSelectedItem().toString();
                Log.i(TAG, device_path);
                uartManager.changeDeviceAndBaudRate(device_path, 0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        baud_rate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = baud_rate.getSelectedItem().toString();
                Log.i(TAG, item);
                uartManager.changeDeviceAndBaudRate(null, Integer.parseInt(item));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        buttonRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uartManager.readData(new UARTManager.OnDataReceiveListener() {
                    @Override
                    public void invoke(String s) {
                        String log = getResources().getString(R.string.log);
                        textView.setText(log + s);
                    }
                });
            }
        });

        buttonWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editable text = editText.getText();
                if (text != null && !text.toString().isEmpty()) {
                    String buffer = text.toString();
                    uartManager.writeData(buffer);
                } else {
                    Toast.makeText(UARTTestActivity.this, R.string.please_input_data, Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void setSpinnerItemSelectedByValue(Spinner spinner,String value){
        SpinnerAdapter apsAdapter= spinner.getAdapter();
        int k= apsAdapter.getCount();
        for(int i=0;i<k;i++){
            if(value.equals(apsAdapter.getItem(i).toString())){
                spinner.setSelection(i,true);
                break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uartManager.release();
    }
}
