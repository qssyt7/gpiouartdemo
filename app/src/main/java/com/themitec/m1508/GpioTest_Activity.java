package com.themitec.m1508;


import android.os.Bundle;
import android.app.Activity;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobiletek.gpio.GPIOManager;
import com.themitec.m1508.R;

public class GpioTest_Activity extends Activity implements OnClickListener {

    private static final String TAG = "GpioTest_Activity";
    private Button gpio_pullup, gpio_pulldown, gpio_read;
    private EditText gpio_edit;
    private TextView gpio_status;
    private TextView valid_gpios;

    //private String[] mGPIOs = {0,1,2,3,4,5,12,13,16,17,18,19,20,21,22,23,33,36,42,44,45,46,48,59,63,66,87,88,89,90,91,92,93,96,97,98,99,128,134,135,136,137,138,140,141};

    String ReadControl = "read";
    String WriteControl = "write";
    String DirectionIN = "in";
    String DirectionOUT = "out";
    int pullUp = 1;
    int pullDown = 0;
    int readValue = 2;

    static int[] gpio_list = {0,1,2,3,4,5,12,13,16,17,18,19,20,21,22,23,33,36,42,44,45,46,48,59,63,66,87,88,89,90,91,92,93,96,97,98,99,128,134,135,136,137,138,140,141};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gpio_test_activity);

        Log.d(TAG, "go to the onCreate() method");

        gpio_pullup = (Button) findViewById(R.id.gpio_pullup);
        gpio_pulldown = (Button) findViewById(R.id.gpio_pulldown);
        gpio_read = (Button) findViewById(R.id.gpio_read);
        gpio_status = (TextView) findViewById(R.id.gpio_status);
        gpio_edit = (EditText) findViewById(R.id.gpio_edit);
        valid_gpios = (TextView) findViewById(R.id.valid_gpios);

        gpio_pullup.setOnClickListener(this);
        gpio_pulldown.setOnClickListener(this);
        gpio_read.setOnClickListener(this);
        gpio_edit.setOnClickListener(this);

    }


    public void PullUpGpio() {
        Log.d(TAG, "go to the PullUpGpio() method");
        String gpioEditValue = "";
        gpioEditValue = gpio_edit.getText().toString();
        int inputGpio= Integer.parseInt(gpioEditValue);

        for (int i = 0; i < gpio_list.length; i++) {
            if (gpio_list[i] == inputGpio) {
                GPIOManager gpioManager = GPIOManager.getInstance(this);
                gpioManager.GpioControl(WriteControl, inputGpio, DirectionOUT, pullUp);
                return;
            }
        }
        Toast.makeText(getApplicationContext(), R.string.valid_chars, Toast.LENGTH_SHORT).show();
    }

    public void PullDownGpio() {
        Log.d(TAG, "go to the PullDownGpio() method");
        String gpioEditValue = "";
        gpioEditValue = gpio_edit.getText().toString();
        int inputGpio= Integer.parseInt(gpioEditValue);

        for (int i = 0; i < gpio_list.length; i++) {
            if (gpio_list[i] == inputGpio) {
                GPIOManager gpioManager = GPIOManager.getInstance(this);
                gpioManager.GpioControl(WriteControl, inputGpio, DirectionOUT, pullDown);
                return;
            }
        }
        Toast.makeText(getApplicationContext(), R.string.valid_chars, Toast.LENGTH_SHORT).show();
    }

    public void SearchGpioStatus() {
        Log.d(TAG, "go to the SearchValidGPIOs() method");
        String result = "";
        String gpioEditValue = "";
        gpioEditValue = gpio_edit.getText().toString();
        int inputGpio= Integer.parseInt(gpioEditValue);

        for (int i = 0; i < gpio_list.length; i++) {
            if (gpio_list[i] == inputGpio) {
                GPIOManager gpioManager = GPIOManager.getInstance(this);
                result = gpioManager.GpioControl(ReadControl, inputGpio, DirectionIN, readValue);
                String text = gpioEditValue + "(" + result + ")";
                gpio_status.setText(text);
                return;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gpio_pulldown:
                if (gpio_edit.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),R.string.gpio_edit_hint, Toast.LENGTH_SHORT).show();
                } else {
                    PullDownGpio();
                }
                break;

            case R.id.gpio_pullup:
                if (gpio_edit.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),R.string.gpio_edit_hint, Toast.LENGTH_SHORT).show();
                } else {
                    PullUpGpio();
                }
                break;

            case R.id.gpio_read:
                String pull_up_status = this.getString(R.string.pull_up_status);
                String pull_down_status = this.getString(R.string.pull_down_status);
                if (gpio_edit.getText().toString().equals("") || Integer.parseInt(gpio_edit.getText().toString()) > 211) {
                    Toast.makeText(getApplicationContext(),R.string.valid_chars, Toast.LENGTH_SHORT).show();
                } else {
                    SearchGpioStatus();
                }
                break;
            default:
                break;
        }

    }
}
