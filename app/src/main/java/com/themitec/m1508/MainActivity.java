package com.themitec.m1508;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.themitec.m1508.R;

public class MainActivity extends Activity implements View.OnClickListener {
    private Button gpio_test, serial_test, serial_test_hex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gpio_test = (Button) findViewById(R.id.gpio_test);
        serial_test = (Button) findViewById(R.id.serial_test);
        serial_test_hex = (Button)findViewById(R.id.serial_test_HEX);
        gpio_test.setOnClickListener(this);
        serial_test.setOnClickListener(this);
        serial_test_hex.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gpio_test:
                Intent gpio_intent = new Intent(MainActivity.this,
                        GpioTest_Activity.class);
                startActivity(gpio_intent);
                break;

            case R.id.serial_test:
                Intent intent = new Intent(MainActivity.this, UARTTestActivity.class);
                startActivity(intent);
                break;

            case R.id.serial_test_HEX:
                Intent intent_hex = new Intent(MainActivity.this, UARTHEXActivity.class);
                startActivity(intent_hex);
                break;

            default:
                break;
        }
    }
}
